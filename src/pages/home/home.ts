import { Component } from '@angular/core';
import { NavController,
         Platform,
         ToastController } from 'ionic-angular';
import { FileOpener } from '@ionic-native/file-opener';
import { File } from '@ionic-native/file';
// Providers
import { Base64PdfProvider } from '../../providers/base64-pdf/base64-pdf';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  private stringPdf: string;
  private gettingPdf: boolean;
  private gotPdf: boolean;
  private writingPdf: boolean;
  private writtenPdf: boolean;
  private contentType: string;
  private fileLoc: string;

  constructor(public navCtrl: NavController,
              public platform: Platform,
              public toastCtrl: ToastController,
              public pdfService: Base64PdfProvider,
              public fileOpener: FileOpener,
              public file: File) {
    this.contentType = 'application/pdf';
  }

  initStatus() {
    this.gettingPdf = false;
    this.gotPdf = false;
    this.writingPdf = false;
    this.writtenPdf = false;
  }

  doRefresh(ev: any) {
    setTimeout(() => {
      // Simulated waiting time.
      ev.complete();
      this.initStatus();
      this.initStringPdf();
    }, 1000);
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter');
    this.initStatus();
    this.initStringPdf();
  }

  initStringPdf() {
    this.gettingPdf = true;
    this.pdfService.getBase64Pdf().then(data => {
      console.log("The PDF as base64 string was retrieved succesfully!");
      this.stringPdf = String(data);
      this.gettingPdf = false;
      this.gotPdf = true;
    });
  }

  presentToast(text: string) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 3000,
      position: 'bottom',
      showCloseButton: false
    });
    toast.present();
  }

  // Delgado, C. (August 11th, 2016). How to save a PDF from a base64 string on
  // the device with Cordova. Retrieved from:
  // https://ourcodeworld.com/articles/read/230/how-to-save-a-pdf-from-a-base64-string-on-the-device-with-cordova
  //
  // Some functions WERE REPLACE to do them compatible.
  /**
   * Convert a base64 string in a Blob according to the data and contentType.
   * 
   * @param b64Data {String} Pure base64 string without contentType
   * @param contentType {String} the content type of the file i.e (application/pdf - text/plain)
   * @param sliceSize {Int} SliceSize to process the byteCharacters
   * @see http://stackoverflow.com/questions/16245767/creating-a-blob-from-a-base64-string-in-javascript
   * @return Blob
   */
  b64toBlob(b64Data, contentType, sliceSize=512) {
    contentType = contentType || '';
    sliceSize = sliceSize || 512;

    let byteCharacters = atob(b64Data);
    let byteArrays = [];

    for (let offset = 0; offset < byteCharacters.length; offset += sliceSize) {
      let slice = byteCharacters.slice(offset, offset + sliceSize);

      let byteNumbers = new Array(slice.length);
      for (let i = 0; i < slice.length; i++) {
        byteNumbers[i] = slice.charCodeAt(i);
      }

      let byteArray = new Uint8Array(byteNumbers);

      byteArrays.push(byteArray);
    }

    let blob = new Blob(byteArrays, {type: contentType});
    return blob;
  }

  /**
   * Create a PDF file according to its database64 content only.
   * 
   * @param folderpath {String} The folder where the file will be created
   * @param filename {String} The name of the file that will be created
   * @param content {Base64 String} Important : The content can't contain the following string (data:application/pdf;base64). Only the base64 string is expected.
   */
  savebase64AsPDF(folderpath, filename, content, contentType) {
    this.writingPdf = true;
    this.fileLoc = folderpath + filename;
    // Convert the base64 string in a Blob
    let  DataBlob = this.b64toBlob(content,contentType);
    
    console.log("Starting to write the file :3");
    // window.resolveLocalFileSystemURL(folderpath, function(dir) {
    this.file.resolveDirectoryUrl(folderpath).then(dir => {
      console.log("Access to the directory granted succesfully");
      // dir.getFile(filename, {create:true}, function(file) {
      this.file.getFile(dir, filename, { create: true }).then(file => {
        console.log("File created succesfully.");
        file.createWriter(fileWriter => {
          console.log("Writing content to file");
          fileWriter.write(DataBlob);
          this.presentToast("El archio fue creado/reescrito exitosamente.");
          this.writtenPdf = true;
          this.writingPdf = false;
        }, function() {
            console.log('Unable to save file in path ' + folderpath);
            this.writingPdf = false;
        });
      }).catch(err => {
        console.log(err);
        this.writingPdf = false;
      });
    }).catch(err => {
      console.log(err);
      this.writingPdf = false;
      this.presentToast("Directorio inexistente o imposible acceder a \u00e9l.");
    });
  }

  writePdf() {
    this.platform.ready().then(() => {
      // Remember to execute this after the onDeviceReady event

      // If your base64 string contains "data:application/pdf;base64,"" at the beginning, keep reading.
      let myBase64 = this.stringPdf;
      // if cordova.file is not available use instead :
      // let folderpath = "file:///storage/emulated/0/";
      // var folderpath = cordova.file.externalRootDirectory;
      let folderpath: string;
      if (this.platform.is('ios')) {
        folderpath = this.file.tempDirectory;
      } else {
        // It DOESN'T WORK with File.cacheDirectory.
        folderpath = this.file.externalCacheDirectory;
      }
      console.log(folderpath);
      let filename = "helloWorld.pdf";

      this.savebase64AsPDF(folderpath, filename, myBase64, this.contentType);
    });
  }

  openPdf() {
    this.fileOpener.open(this.fileLoc, this.contentType).then(() => {
      console.log("The file was opened.");
    }).catch(err => {
      console.log("Error trying to open the file:", err);
      this.presentToast(err);
    });
  }
}